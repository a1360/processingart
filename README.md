1. You need to convert to mkv first
   ffmpeg -framerate 60 -start_number 0601 -i 'screen-%04d.tif' -an -crf 0 -b:v 0 OUT.mkv
2. Convert to gif/webm format:
   ffmpeg -i OUT.mkv -c:v libvpx -vf scale=800x580 -an -crf 15 -ss 00:00:00.000 -to 00:00:15.000 -b:v 1500K OUT_15.gif

ffmpeg -i OUT.mkv -c:v libvpx -an -crf 4 -b:v 1500K -vf scale=800x580 OUT_4.webm
ffmpeg -i OUT.mkv -vf scale=800x580 -an -crf 15 -ss 00:00:00.000 -to 00:00:15.000 -b:v 1500K OUT_15.gif
