package processing.art.effect.graphic

import processing.core.PGraphics
import kotlin.math.ceil
import kotlin.random.Random
import kotlin.random.nextInt

class MatrixGraphicEffect(
    private val speedRange: IntRange,
    private val textColor: Int,
    private val textHighlightColor: Int,
    private val blendLength: IntRange,
    private val rainLength: IntRange,
    private val characterPoviderFactory: CharacterProviderFactory,
    private val textSize: Float = 20f,
    private val random: Random = Random(System.currentTimeMillis()),
    private val hardcodedColumnFadeLength: Map<Int, Int> = emptyMap(),
) : GraphicEffect {

    private val columns = ArrayList<SymbolColumn>()

    override fun setup(graphic: PGraphics) {
        graphic.textSize(textSize)
        val characterWidth = graphic.textWidth(characterPoviderFactory.requestProviderFor(0, 0).requestCharacterInNewFrame())
        val characterHeight = graphic.textLeading
        val numberOfColumns = ceil(graphic.width / characterWidth).toInt()
        val numberOfRows = ceil(graphic.height / characterHeight).toInt() + 1

        repeat(numberOfColumns) {
            columns.add(SymbolColumn(startSymbol = random.nextInt(0, numberOfRows)))
        }

        for (column in 0 until numberOfColumns) {
            val alphaStep = 255 / (hardcodedColumnFadeLength[column] ?: random.nextInt(rainLength))
            val blendStep = 1f / random.nextInt(blendLength)
            val speed = random.nextDouble(speedRange.first.toDouble(), speedRange.last + 1.0).toFloat()
            for (row in 0 until numberOfRows) {
                val symbolColumn = columns[column]
                val rowsAwayFromStart = if (row > symbolColumn.startSymbol) {
                    symbolColumn.startSymbol + numberOfRows - row
                } else {
                    symbolColumn.startSymbol - row
                }
                val alpha = 255 - rowsAwayFromStart * alphaStep
                val color = graphic.lerpColor(textColor, textHighlightColor, 1f - rowsAwayFromStart * blendStep)
                symbolColumn.symbols.add(
                    Symbol(
                        x = characterWidth * column,
                        y = characterHeight * row.toFloat(),
                        characterHeight = characterHeight.toInt(),
                        textColor = color,
                        speed = speed,
                        alpha = alpha,
                        characterProvider = characterPoviderFactory.requestProviderFor(column, rowsAwayFromStart),
                    )
                )
            }
        }
    }

    override fun draw(graphic: PGraphics) {
        for (c in columns) {
            for (symbol in c.symbols) {
                symbol.draw(graphic)
            }
        }
    }

    private class Symbol(
        private val x: Float,
        private var y: Float,
        private val characterHeight: Int,
        private val textColor: Int,
        private val speed: Float,
        private val alpha: Int,
        private val characterProvider: CharacterProvider,
    ) {

        fun draw(graphic: PGraphics) {
            advance(graphic.height, speed)
            graphic.fill(
                textColor,
                alpha.toFloat(),
            )
            graphic.text(characterProvider.requestCharacterInNewFrame(), x, y)
        }

        private fun advance(height: Int, speed: Float) {
            if (y < height + characterHeight) {
                y += speed;
            } else {
                y = 0f;
            }
        }
    }

    private data class SymbolColumn(
        val symbols: ArrayList<Symbol> = ArrayList(),
        val startSymbol: Int,
    )
}

fun interface CharacterProviderFactory {

    fun requestProviderFor(column: Int, index: Int): CharacterProvider
}

fun interface CharacterProvider {

    fun requestCharacterInNewFrame(): Char
}

class RandomAsianCharacterProvider(
    private val random: Random,
    private val characterSwitchInterval: IntRange,
) : CharacterProvider {

    private var currentSwitchValue = random.nextInt(characterSwitchInterval)
    private var frameCount = 0
    private var value = random.nextInt(0x30A0, 0x3100).toChar()

    override fun requestCharacterInNewFrame(): Char {
        frameCount++
        if (frameCount % currentSwitchValue == 0) {
            currentSwitchValue = random.nextInt(characterSwitchInterval)
            value = random.nextInt(0x30A0, 0x3100).toChar()
        }
        return value
    }
}

class WordCharacterProviderFactory(
    private val word: String,
) : CharacterProviderFactory {

    override fun requestProviderFor(column: Int, index: Int): CharacterProvider =
        WordCharacterProvider(index, word)
}

class WordCharacterProvider(
    index: Int,
    word: String,
) : CharacterProvider {

    private val value: Char = word[index % word.length]

    override fun requestCharacterInNewFrame(): Char = value
}
