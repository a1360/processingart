package processing.art.effect.graphic

import processing.core.PGraphics

interface GraphicEffect {

    fun setup(graphic: PGraphics)

    fun draw(graphic: PGraphics)
}