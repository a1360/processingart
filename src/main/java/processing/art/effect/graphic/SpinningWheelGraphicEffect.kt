package processing.art.effect.graphic

import processing.core.PApplet
import processing.core.PGraphics

class SpinningWheelGraphicEffect(
    centerX: Float,
    centerY: Float,
    drawSpeed: Float = 1.25f,
    shrinkSpeed: Float = 0.5f,
    strokeWidth: Float = 8f,
    arcLength: Float = 25f,
    spikeStages: List<Float> = listOf(0.40f, 0.60f),
    startingRadius: Float = 600f,
    distanceBetweenCircles: Float = 150f,
    private val numberOfSpikes: Int = 10,
    spikeSpeedModifier: Float = 1f,
    startingArc: Float = 0f,
    radiusLimit: Float = startingRadius,
    fadeOutRange: IntRange = (startingRadius.toInt() - distanceBetweenCircles.toInt() * 2) until startingRadius.toInt() - (0.5 * distanceBetweenCircles).toInt(),
    strokeColor: Int,
) : GraphicEffect {

    init {
        check(spikeStages.size == 2) { "Incorrect spikeStages size. Must be 3." }
    }

    private val config = SharedConfig(
        strokeColor = strokeColor,
        strokeWidth = strokeWidth,
        drawSpeed = drawSpeed,
        shrinkSpeed = shrinkSpeed,
        radiusLimit = radiusLimit,
        fadeOutRange = fadeOutRange,
    )

    private val baseCircle = Circle(
        config = config,
        x = centerX,
        y = centerY,
        startingRadius = startingRadius,
        startingArc = startingArc,
    )

    private val baseSpike = Spike(
        config = config,
        centerX = centerX,
        centerY = centerY,
        totalHeight = distanceBetweenCircles,
        startingRadius = startingRadius,
        arcLength = arcLength,
        spikeStages = spikeStages,
        angle = 0f,
        percentageComplete = 0f,
        spikeSpeedModifier = spikeSpeedModifier,
    )

    private val sun = List(PApplet.floor(startingRadius / distanceBetweenCircles)) {
        val startRadius = startingRadius - distanceBetweenCircles * it
        baseCircle.copy(radius = startRadius) to spikes(startRadius)
    }.let { Sun(it) }

    private fun spikes(radius: Float): List<Spike> {
        val angleStep = 360f / numberOfSpikes
        return List(numberOfSpikes) {
            baseSpike.copy(radius = radius, angle = it * angleStep)
        }
    }

    override fun setup(graphic: PGraphics) {
    }

    override fun draw(graphic: PGraphics) {
        sun.draw(graphic)
    }
}

private data class SharedConfig(
    val strokeColor: Int,
    val strokeWidth: Float = 6f,
    val drawSpeed: Float = 1f,
    val shrinkSpeed: Float = 0.5f,
    val radiusLimit: Float,
    val fadeOutRange: IntRange,
)

private data class Sun(
    val circles: List<Pair<Circle, List<Spike>>>
) {

    private val prevCircles = circles
        .map { it.first }
        .mapIndexed { index, circle -> if (index == 0) { circles.last().first } else { circles[index - 1].first } }

    fun draw(graphic: PGraphics) {
        circles.forEachIndexed { index, pair ->
            val (circle, spikes) = pair
            circle.shrink()
            circle.incrementDraw()
            circle.draw(graphic)

            val circleToFollow = if (circle.config.shrinkSpeed > 0) {
                prevCircles[index]
            } else {
                circle
            }
            spikes.forEach { spike ->
                spike.shrink()
                if (spike.radius > circle.radius) {
                    spike.radius = circle.radius
                }
                if (
                    spike.angle > circleToFollow.startingArc && (circleToFollow.arc + circleToFollow.startingArc) >= spike.angle ||
                    spike.angle < circleToFollow.startingArc && (circleToFollow.arc + circleToFollow.startingArc) >= spike.angle + 360f ||
                    (circleToFollow.arc - circleToFollow.startingArc) >= 360f
                ) {
                    spike.incrementDraw()
                    spike.draw(graphic)
                }
            }
        }
    }
}

private data class Spike(
    val config: SharedConfig,
    val centerX: Float,
    val centerY: Float,
    val totalHeight: Float,
    val startingRadius: Float,
    val arcLength: Float,
    val spikeStages: List<Float>,
    val spikeSpeedModifier: Float,
    var angle: Float,
    var radius: Float = startingRadius,
    var percentageComplete: Float = 0f,
) {

    fun incrementDraw() {
        if (percentageComplete < 1f) {
            percentageComplete += (config.drawSpeed / 100) * spikeSpeedModifier
        }
    }

    fun shrink() {
        val largestRadius = radius + totalHeight
        radius -= config.shrinkSpeed
        if (largestRadius < -1 || radius > config.radiusLimit) {
            radius = if (config.shrinkSpeed > 0) startingRadius else 0f
            percentageComplete = 0f
        }
    }

    fun draw(graphic: PGraphics) {
        if (percentageComplete > 0.001f) {
            drawLength(graphic, PApplet.radians(angle), radius, 0f, spikeStages[0])
        }
        if (percentageComplete > spikeStages[0]) {
            graphic.stroke(config.strokeColor)
            graphic.strokeWeight(config.strokeWidth)
            graphic.noFill()
            val length = angle + PApplet.min(
                arcLength,
                PApplet.map(percentageComplete, spikeStages[0], spikeStages[1], 0f, arcLength)
            )
            val diameter = radius * 2 + totalHeight
            graphic.arc(centerX, centerY, diameter, diameter, PApplet.radians(angle), PApplet.radians(length))
        }
        if (percentageComplete > spikeStages[1]) {
            drawLength(graphic, PApplet.radians(angle + arcLength), radius + totalHeight / 2, spikeStages[1], 1f)
        }
    }

    private fun drawLength(
        applet: PGraphics,
        radianAngle: Float,
        startRadius: Float,
        startPercentage: Float,
        endPercentage: Float,
    ) {
        val safeStartRadius = PApplet.max(0f, startRadius)
        val startX = PApplet.cos(radianAngle) * safeStartRadius
        val startY = PApplet.sin(radianAngle) * safeStartRadius
        val endHeight = startRadius +
                PApplet.min(
                    totalHeight / 2,
                    PApplet.map(percentageComplete, startPercentage, endPercentage, 0f, totalHeight / 2)
                )
        val safeEndHeight = PApplet.max(0f, endHeight)
        val endX = PApplet.cos(radianAngle) * safeEndHeight
        val endY = PApplet.sin(radianAngle) * safeEndHeight

        val shape = applet.createShape()
        shape.beginShape()
        shape.stroke(config.strokeColor)
        shape.strokeWeight(config.strokeWidth)
        shape.vertex(startX, startY)
        shape.vertex(endX, endY)
        shape.endShape()
        applet.shape(shape, centerX, centerY)
    }
}

private data class Circle(
    val config: SharedConfig,
    val x: Float,
    val y: Float,
    val startingRadius: Float,
    val startingArc: Float,
    var arc: Float = 0f,
    var radius: Float = startingRadius,
) {

    fun incrementDraw() {
        if (arc < (360 + startingArc)) {
            arc += config.drawSpeed
        }
    }

    fun shrink() {
        radius -= config.shrinkSpeed
        if (radius < 0 || radius > config.radiusLimit) {
            radius = if (config.shrinkSpeed > 0) startingRadius else 0f
            arc = 0f
        }
    }

    fun draw(applet: PGraphics) {
        applet.stroke(config.strokeColor)
        applet.strokeWeight(config.strokeWidth)
        applet.noFill()
        applet.arc(x, y, radius * 2, radius * 2, PApplet.radians(startingArc), PApplet.radians(arc + startingArc))
    }
}