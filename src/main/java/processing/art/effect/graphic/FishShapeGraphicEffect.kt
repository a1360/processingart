package processing.art.effect.graphic

import processing.art.effect.graphic.GraphicEffect
import processing.core.PGraphics

class FishShapeGraphicEffect(
    private val x: Int,
    private val y: Int,
    private val width: Int,
    private val height: Int,
    private val thickness: Float,
    private val color: Int,
) : GraphicEffect {

    override fun setup(graphic: PGraphics) = Unit

    override fun draw(graphic: PGraphics) {
        graphic.stroke(color)
        graphic.strokeWeight(thickness)

        drawOutline(graphic)
    }

    private fun drawOutline(graphic: PGraphics) {
        val startX = x + width / 2f
        val startY = y.toFloat()
        val midY = startY + height / 3f
        val endY = startY + height

        graphic.line(startX, startY, x.toFloat(), midY)
        graphic.line(x.toFloat(), midY, (x + width).toFloat(), endY)

        graphic.line(startX, startY, (x + width).toFloat(), midY)
        graphic.line((x + width).toFloat(), midY, x.toFloat(), endY)
    }
}
