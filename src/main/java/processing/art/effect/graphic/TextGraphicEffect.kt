package processing.art.effect.graphic

import processing.core.PGraphics

class TextGraphicEffect(
    private val textSize: Float,
    private val text: String,
    private val position: Position,
    private val textColor: Int,
): GraphicEffect {

    private var x = 0f
    private var y = 0f

    override fun setup(graphic: PGraphics) {
        graphic.fill(textColor)
        graphic.textSize(textSize)
        when (position) {
            Position.BOTTOM_RIGHT -> {
                x = graphic.width - graphic.textWidth(text) - 10f
                y = graphic.height - textSize
            }
            Position.CENTER -> {
                x = graphic.width / 2f - graphic.textWidth(text) / 2f
                y = graphic.height / 2 - textSize / 2
            }
        }.let {  }
    }

    override fun draw(graphic: PGraphics) {
        graphic.text(text, x, y)
    }

    enum class Position {
        CENTER, BOTTOM_RIGHT;
    }
}
