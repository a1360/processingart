package processing.art.effect.graphic

import processing.core.PGraphics
import kotlin.math.roundToInt

class SlidingBarsGraphicEffect(
    private val color: Int,
    private val gap: Float,
    private val thickness: Float,
    private val animationSpeed: Float,
) : GraphicEffect {

    private var offset = (gap + thickness) * -1

    override fun setup(graphic: PGraphics) = Unit

    override fun draw(graphic: PGraphics) {
        val width = graphic.width
        val height = graphic.height

        offset += animationSpeed
        if (offset > 0) {
            offset = (gap + thickness) * -1
        }

        graphic.fill(color)
        for (y in offset.roundToInt() until height step (thickness + gap).roundToInt()) {
            graphic.rect(0f, y.toFloat(), width.toFloat(), thickness)
        }
    }
}