package processing.art.effect.graphic

import processing.art.effect.graphic.GraphicEffect
import processing.core.PGraphics

class BackgroundColorGraphicEffect(
    private val color: Int,
) : GraphicEffect {

    override fun setup(graphic: PGraphics) = Unit

    override fun draw(graphic: PGraphics) {
        graphic.background(color)
    }
}
