package processing.art.effect.graphic

import processing.core.PGraphics

class ReflectionGraphicEffect(
    private val orientation: Orientation,
) : GraphicEffect {

    override fun setup(graphic: PGraphics) = Unit

    override fun draw(graphic: PGraphics) {
        val parent = graphic.parent.also { it.loadPixels() }
        graphic.loadPixels()
        when (orientation) {
            is Orientation.Vertical -> verticalReflection(orientation.x, parent.pixels, graphic)
            is Orientation.Horizontal -> horizontalReflection(orientation.y, parent.pixels, graphic)
        }.let { }
        graphic.updatePixels()
    }

    private fun verticalReflection(x: Int, parent: IntArray, graphic: PGraphics) {
        val startX = if (orientation is Orientation.Vertical.Right) 0 else x + 1
        val endX = if (orientation is Orientation.Vertical.Right) x else graphic.width

        for (row in startX until endX) {
            val mirrorRow = graphic.width - row - 1
            for (column in 0 until graphic.height) {
                val currentLocation = row + column * graphic.width
                val mirrorLocation = mirrorRow + column * graphic.width
                graphic.pixels[currentLocation] = parent[mirrorLocation]
            }
        }
    }

    private fun horizontalReflection(y: Int, parent: IntArray, graphic: PGraphics) {
        val startY = if (orientation is Orientation.Horizontal.Down) 0 else y + 1
        val endY = if (orientation is Orientation.Horizontal.Down) y else graphic.height


        for (column in startY until endY) {
            val mirrorColumn = if (orientation is Orientation.Horizontal.Down) graphic.height - column - 1 else y - (column - y)
            if (mirrorColumn >= 0) {
                for (row in 0 until graphic.width) {
                    val currentLocation = row + column * graphic.width
                    val mirrorLocation = row + mirrorColumn * graphic.width
                    graphic.pixels[currentLocation] = parent[mirrorLocation]
                }
            }
        }
    }

    sealed interface Orientation {

        sealed class Vertical(val x: Int) : Orientation {

            class Left(x: Int) : Vertical(x)

            class Right(x: Int) : Vertical(x)
        }

        sealed class Horizontal(val y: Int) : Orientation {

            class Down(y: Int) : Horizontal(y)

            class Up(y: Int) : Horizontal(y)
        }
    }
}