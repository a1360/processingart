package processing.art.effect.graphic

import processing.art.effect.graphic.GraphicEffect
import processing.core.PGraphics
import processing.core.PImage

class BackgroundImageGraphicEffect(
    private val fileName: String,
) : GraphicEffect {

    private lateinit var backgroundImage: PImage

    override fun setup(graphic: PGraphics) {
        backgroundImage = graphic.parent.loadImage(fileName)
    }

    override fun draw(graphic: PGraphics) {
        graphic.image(backgroundImage, 0f, 0f)
    }
}