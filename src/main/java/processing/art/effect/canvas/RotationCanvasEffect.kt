package processing.art.effect.canvas

import processing.core.PGraphics

class RotationCanvasEffect(
    val center: Pair<Int, Int>,
    val type: RotationType,
) : CanvasEffect {

    private var rotation = if (type is RotationType.Spinning) type.startAngleRadians else 0f

    override fun setupCanvas(graphic: PGraphics) {
        calculateRotation()
        graphic.translate(center.first.toFloat(), center.second.toFloat())
        graphic.rotate(rotation)
        graphic.translate(-center.first.toFloat(), -center.second.toFloat())
    }

    private fun calculateRotation() {
        when (type) {
            is RotationType.Fixed -> rotation = type.angleRadians
            is RotationType.Spinning -> rotation += type.rotationSpeed
        }.let {  }
    }

    sealed interface RotationType {

        data class Fixed(val angleRadians: Float) : RotationType

        data class Spinning(val startAngleRadians: Float, val rotationSpeed: Float) : RotationType
    }
}
