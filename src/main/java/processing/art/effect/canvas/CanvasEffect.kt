package processing.art.effect.canvas

import processing.core.PGraphics

interface CanvasEffect {

    fun setupCanvas(graphic: PGraphics)
}