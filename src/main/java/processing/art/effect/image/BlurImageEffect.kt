package processing.art.effect.image

import processing.art.toPixelLocation
import processing.core.PGraphics
import processing.core.PImage
import processing.opengl.PShader

class BlurImageEffect(
    private val blurLevel: Float
) : ImageEffect {

    override fun draw(image: PImage, graphic: PGraphics) {
        val nonBlurred = image.copy()
        image.filter(PShader.BLUR, blurLevel)

        for (row in 0 until image.width) {
            for (column in 0 until image.height) {
                val location = nonBlurred.toPixelLocation(row, column)
                val alpha = graphic.alpha(nonBlurred.pixels[location])
                val black = graphic.color(0f, 0f, 0f, 255f)
                if (alpha > 0 && nonBlurred.pixels[location] != black) {
                    image.pixels[location] = nonBlurred.pixels[location]
                }
            }
        }
    }
}