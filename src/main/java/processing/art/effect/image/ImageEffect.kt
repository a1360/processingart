package processing.art.effect.image

import processing.core.PGraphics
import processing.core.PImage

interface ImageEffect {

    fun draw(image: PImage, graphic: PGraphics)
}