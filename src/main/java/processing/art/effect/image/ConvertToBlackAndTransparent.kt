package processing.art.effect.image

import processing.art.toPixelLocation
import processing.core.PGraphics
import processing.core.PImage

class ConvertToBlackAndTransparent : ImageEffect {

    override fun draw(image: PImage, graphic: PGraphics) {
        for (row in 0 until image.width) {
            for (column in 0 until image.height) {
                val location = image.toPixelLocation(row, column)

                val alpha = graphic.alpha(image.pixels[location])
                val newColor = graphic.color(if (alpha > 0) 255 else 0)

                image.pixels[location] = newColor
            }
        }
    }
}