package processing.art.effect.image

import processing.core.PGraphics
import processing.core.PImage
import processing.art.toPixelLocation
import kotlin.math.roundToInt

class PerlinNoiseImageEffect(
    private val x: Int,
    private val y: Int,
    private val width: Int,
    private val height: Int,
    private val scale: Float,
    private val speed: Float,
    private val thickness: Int,
) : ImageEffect {

    private var offsetY = 0f

    override fun draw(image: PImage, graphic: PGraphics) {
        offsetY += speed
        val tempRow = IntArray(width)
        var noiseX = 0f
        var offsetX = 0
        var rowCount = 0
        for (column in y until (y + height - 1)) {
            if (rowCount % thickness == 0) {
                rowCount++
                offsetX++
                noiseX = graphic.parent.noise(offsetX.toFloat(), offsetY) * scale - scale / 2
            }
            for (row in x until (x + width - 1)) {
                val shiftedRow = row + noiseX
                val blank = graphic.color(0f, 0f, 0f, 0f)
                val shiftedLocationColor = if (
                    shiftedRow >= 0 && shiftedRow < image.width
                ) {
                    image.pixels[image.toPixelLocation(shiftedRow.roundToInt(), column)]
                } else {
                    blank
                }
                tempRow[row - x] = shiftedLocationColor
            }
            for (row in x until (x + width)) {
                val location = image.toPixelLocation(row, column)
                val newColor = tempRow[row - x]
                image.pixels[location] = newColor
            }
            rowCount++
        }
    }
}