package processing.art.effect.image

import processing.core.PGraphics
import processing.core.PImage
import kotlin.math.*

class LinearFunctionEraseImageEffect(
    pointOne: Pair<Float, Float>,
    pointTwo: Pair<Float, Float>,
    private val eraseDirection: EraseDirection,
    private val fadeLength: Float,
) : ImageEffect {

    private val m: Float = when (eraseDirection) {
        EraseDirection.TO_RIGHT -> (pointTwo.first - pointOne.first) / (pointTwo.second - pointOne.second)
        EraseDirection.ABOVE,
        EraseDirection.BELOW -> (pointTwo.second - pointOne.second) / (pointTwo.first - pointOne.first)
    }

    private val c: Float = when (eraseDirection) {
        EraseDirection.TO_RIGHT -> pointTwo.first - pointTwo.second * m
        EraseDirection.ABOVE,
        EraseDirection.BELOW -> pointTwo.second - pointTwo.first * m
    }

    override fun draw(image: PImage, graphic: PGraphics) {
        when (eraseDirection) {
            EraseDirection.TO_RIGHT -> {
                for (column in 0 until image.height) {
                    val startRow = mapToLine(column)
                    for (row in startRow until image.width) {
                        updateAlpha(image, graphic, row, column)
                    }
                }
            }
            EraseDirection.ABOVE -> {
                for (row in 0 until image.width) {
                    val startColumn = mapToLine(row)
                    if (startColumn >= 0 && startColumn < image.height) {
                        for (column in 0 until startColumn) {
                            updateAlpha(image, graphic, row, column)
                        }
                    }
                }
            }
            EraseDirection.BELOW -> {
                for (row in 0 until image.width) {
                    val startColumn = mapToLine(row)
                    if (startColumn >= 0 && startColumn < image.height) {
                        for (column in startColumn until image.height) {
                            updateAlpha(image, graphic, row, column)
                        }
                    }
                }
            }
        }.let {  }
    }

    private fun updateAlpha(image: PImage, graphic: PGraphics, row: Int, column: Int) {
        val loc = row + column * image.width
        val red = graphic.red(image.pixels[loc])
        val blue = graphic.blue(image.pixels[loc])
        val green = graphic.green(image.pixels[loc])
        val originalAlpha = graphic.alpha(image.pixels[loc])

        val alpha = calculateAlpha(row, column, originalAlpha)
        if (originalAlpha > 0) {
            val newColor = graphic.color(red, green, blue, alpha)
            image.pixels[loc] = newColor
        }
    }

    private fun calculateAlpha(x: Int, y: Int, originalAlpha: Float): Float =
        when (eraseDirection) {
            EraseDirection.TO_RIGHT -> calculateDistance(y, x)
            EraseDirection.ABOVE,
            EraseDirection.BELOW -> calculateDistance(x, y)
        }.let { originalAlpha - max(0f, min(originalAlpha, it * 255 / fadeLength)) }

    // calculate perpendicular line that passes through the point
    // m * m2 = -1
    // m2 = - 1 / m
    // Calculate the missing c value
    // m2 * x + c2 = y
    // c2 = y - m2 * x
    // find the intersecting point
    // m2 * x + c2 = m * x + c
    // m2 * x - m * x = c - c2
    // x (m2 - m) = c - c2
    // x = (c - c2) / (m2 - m)
    private fun calculateDistance(x: Int, y: Int): Float {
        val m2 = - 1 / m
        val c2 = y - m2 * x
        val nearestX = (c - c2) / (m2 - m)
        val nearestY = mapToLine(nearestX.roundToInt()).toFloat()
        return sqrt(abs(nearestX - x).pow(2) + abs(nearestY - y).pow(2))
    }

    private fun mapToLine(x: Int): Int =
        ceil(x * m + c).roundToInt()

    enum class EraseDirection {
        ABOVE, BELOW, TO_RIGHT;
    }
}
