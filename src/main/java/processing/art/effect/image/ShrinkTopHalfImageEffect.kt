package processing.art.effect.image

import processing.art.toPixelLocation
import processing.core.PGraphics
import processing.core.PImage
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.Transparency
import java.awt.image.BufferedImage
import kotlin.math.roundToInt

/**
 *
 * Intended to be used with the ReflectionGraphicEffect class to shrink the reflected image
 *
 * @see processing.art.effect.graphic.ReflectionGraphicEffect
 */
class ShrinkTopHalfImageEffect(
    private val shrinkHeightBy: Float
) : ImageEffect {

    override fun draw(image: PImage, graphic: PGraphics) {
        val halfOriginalHeight = image.height / 2
        val shrunkHeight = (image.height * shrinkHeightBy).roundToInt()
        val shrunk = shrinkImage(image.native as BufferedImage, image.width, shrunkHeight)
        val new = PImage(shrunk)

        val blankPixel = graphic.color(0f, 0f, 0f ,0f)
        for (row in 0 until image.width) {
            var startShrunkHeight = shrunkHeight / 2
            for (column in 0 until image.height) {
                val location = image.toPixelLocation(row, column)
                val shrunkLocation = new.toPixelLocation(row, startShrunkHeight)
                if (column >= halfOriginalHeight && shrunkLocation < new.pixels.size) {
                    image.pixels[location] = new.pixels[shrunkLocation]
                    startShrunkHeight++
                } else {
                    image.pixels[location] = blankPixel
                }
            }
        }
    }

    // copied from PImage.resize
    private fun shrinkImage(
        img: BufferedImage,
        targetWidth: Int, targetHeight: Int
    ): BufferedImage {
        val type =
            if (img.transparency == Transparency.OPAQUE) BufferedImage.TYPE_INT_RGB else BufferedImage.TYPE_INT_ARGB
        var outgoing = img
        var scratchImage: BufferedImage? = null
        var g2: Graphics2D? = null
        var prevW = outgoing.width
        var prevH = outgoing.height
        val isTranslucent = img.transparency != Transparency.OPAQUE

        // Use multi-step technique: start with original size, then scale down in
        // multiple passes with drawImage() until the target size is reached
        var w = img.width
        var h = img.height
        do {
            if (w > targetWidth) {
                w /= 2
                // if this is the last step, do the exact size
                if (w < targetWidth) {
                    w = targetWidth
                }
            } else if (targetWidth >= w) {
                w = targetWidth
            }
            if (h > targetHeight) {
                h /= 2
                if (h < targetHeight) {
                    h = targetHeight
                }
            } else if (targetHeight >= h) {
                h = targetHeight
            }
            if (scratchImage == null || isTranslucent) {
                // Use a single scratch buffer for all iterations and then copy
                // to the final, correctly-sized image before returning
                scratchImage = BufferedImage(w, h, type)
                g2 = scratchImage.createGraphics()
            }
            g2!!.setRenderingHint(
                RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR
            )
            g2!!.drawImage(outgoing, 0, 0, w, h, 0, 0, prevW, prevH, null)
            prevW = w
            prevH = h
            outgoing = scratchImage
        } while (w != targetWidth || h != targetHeight)
        g2?.dispose()

        // If we used a scratch buffer that is larger than our target size,
        // create an image of the right size and copy the results into it
        if (targetWidth != outgoing.width ||
            targetHeight != outgoing.height
        ) {
            scratchImage = BufferedImage(targetWidth, targetHeight, type)
            g2 = scratchImage.createGraphics()
            g2.drawImage(outgoing, 0, 0, null)
            g2.dispose()
            outgoing = scratchImage
        }
        return outgoing
    }
}
