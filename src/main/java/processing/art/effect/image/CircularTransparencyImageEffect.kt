package processing.art.effect.image

import processing.core.PApplet
import processing.core.PGraphics
import processing.core.PImage

class CircularTransparencyImageEffect(
    private val centerX: Float,
    private val centerY: Float,
    private val fullyBlurAtDistance: Float,
) : ImageEffect {

    override fun draw(image: PImage, graphic: PGraphics) {
        image.applyTransparencyClip(graphic)
    }

    private fun PImage.applyTransparencyClip(graphic: PGraphics) {
        for (row in 0 until width) {
            for (column in 0 until height) {
                val loc = row + column * width

                val red = graphic.red(pixels[loc])
                val blue = graphic.blue(pixels[loc])
                val green = graphic.green(pixels[loc])
                val originalAlpha = graphic.alpha(pixels[loc])

                if (originalAlpha > 50) {
                    val distance =
                        PApplet.sqrt(
                            PApplet.abs(centerX - row) * PApplet.abs(centerX - row) + PApplet.abs(centerY - column) * PApplet.abs(
                                centerY - column
                            )
                        )
                    val alpha =
                        PApplet.map(PApplet.min(distance, fullyBlurAtDistance), fullyBlurAtDistance, 0f, 0f, 255f)

                    pixels[loc] = graphic.color(red, green, blue, alpha)
                }
            }
        }
    }
}