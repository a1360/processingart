package processing.art.effect.image

import processing.core.PGraphics
import processing.core.PImage
import processing.art.effect.graphic.GraphicEffect
import processing.art.piece.GraphicComponent

class MaskImageEffect(
    private val component: GraphicComponent
) : ImageEffect {

    constructor(graphicEffect: GraphicEffect) : this(GraphicComponent(graphicEffect = graphicEffect))

    private var mask: PImage? = null

    override fun draw(image: PImage, graphic: PGraphics) {
        image.mask(getOrCreateMask(graphic))
    }

    private fun getOrCreateMask(graphic: PGraphics): PImage {
        var safeMask = mask
        if (safeMask == null) {
            safeMask = drawMask(graphic.parent.createGraphics(graphic.width, graphic.height))
            mask = safeMask
        }
        return safeMask
    }

    private fun drawMask(graphic: PGraphics): PImage {
        graphic.beginDraw()

        component.canvasEffects.forEach { it.setupCanvas(graphic) }

        component.graphicEffect?.setup(graphic)
        component.graphicEffect?.draw(graphic)

        graphic.endDraw()

        val image = graphic.get()
        component.imageEffects.forEach { it.draw(image, graphic) }

        return image
    }
}