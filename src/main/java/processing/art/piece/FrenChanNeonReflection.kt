package processing.art.piece

import processing.art.effect.graphic.BackgroundImageGraphicEffect
import processing.art.effect.graphic.ReflectionGraphicEffect
import processing.art.effect.graphic.SlidingBarsGraphicEffect
import processing.art.effect.image.BlurImageEffect
import processing.art.effect.image.ConvertToBlackAndTransparent
import processing.art.effect.image.LinearFunctionEraseImageEffect
import processing.art.effect.image.MaskImageEffect
import processing.art.effect.image.PerlinNoiseImageEffect
import processing.art.effect.image.ShrinkTopHalfImageEffect
import processing.core.PApplet

class FrenChanNeonReflection : Piece(
    pieceWidth = 265,
    pieceHeight = 1000,
    record = null,
    effectsProvider = {
        listOf(
            GraphicComponent(
                graphicEffect = SlidingBarsGraphicEffect(
                    color = it.color(235,31,84),
                    gap = 5f,
                    thickness = 10f,
                    animationSpeed = 0.25f,
                ),
                imageEffects = listOf(
                    MaskImageEffect(
                        GraphicComponent(
                            graphicEffect = BackgroundImageGraphicEffect("pictures/frenchan_f.png"),
                            imageEffects = listOf(
                                ConvertToBlackAndTransparent()
                            )
                        )
                    ),
                    BlurImageEffect(16f),
                )
            ),
            GraphicComponent(
                graphicEffect = ReflectionGraphicEffect(
                    orientation = ReflectionGraphicEffect.Orientation.Horizontal.Up(450)
                ),
                imageEffects = listOf(
                    PerlinNoiseImageEffect(
                        x = 0,
                        y = 500,
                        width = 265,
                        height = 500,
                        scale = 30f,
                        speed = 0.02f,
                        thickness = 3,
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 8f to 457f,
                        pointTwo = 256f to 477f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.BELOW,
                        fadeLength = 480f,
                    ),
                    ShrinkTopHalfImageEffect(shrinkHeightBy = 4f / 5f)
                )
            ),
        )
    }
)

fun main() {
    val sketch = FrenChanNeonReflection()
    PApplet.runSketch(arrayOf("FrenChanNeonReflection"), sketch)
}
