package processing.art.piece

import processing.art.effect.canvas.CanvasEffect
import processing.core.PApplet
import processing.art.effect.graphic.GraphicEffect
import processing.art.effect.image.ImageEffect
import processing.core.PGraphics
import processing.core.PImage
import java.io.File

open class Piece(
    private val pieceWidth: Int,
    private val pieceHeight: Int,
    private val record: IntRange?,
    effectsProvider: (PApplet) -> List<GraphicComponent>,
    private val overrideConvertToWebmParams: String? = null,
) : PApplet() {

    private val effects by lazy { effectsProvider(this) }
    private val graphics by lazy {
        List(effects.size) {
            createGraphics(pieceWidth, pieceHeight)
                .also { it.smooth(8) }
        }
    }
    private var isRecording = false
    private var saveFrameCount = 1

    override fun settings() {
        smooth(8)
        size(pieceWidth, pieceHeight)
    }

    override fun setup() {
        graphics.forEachIndexed { index, graphic ->
            graphic.beginDraw()
            effects[index].graphicEffect?.setup(graphic)
            graphic.endDraw()
        }
        deleteOutputDirectory()
    }

    override fun draw() {
        drawGraphicComponents()
        performRecording()
    }

    private fun drawGraphicComponents() {
        clear()
        graphics.forEachIndexed { index, graphic ->
            graphic.clear()

            graphic.beginDraw()
            val component = effects[index]
            val image = drawGraphicComponent(index, component, graphic)
            graphic.endDraw()

            image(image, 0f, 0f)
        }
    }

    open fun drawGraphicComponent(index: Int, component: GraphicComponent, graphic: PGraphics): PImage {
        val canvasEffects = effects[index].canvasEffects
        canvasEffects.forEach { it.setupCanvas(graphic) }

        val graphicEffect = effects[index].graphicEffect
        graphicEffect?.draw(graphic)

        val imageEffects = effects[index].imageEffects
        val image = graphic.get()
        imageEffects.forEach { it.draw(image, graphic) }

        return image
    }

    private fun performRecording() {
        val record = record
        if (record != null) {
            if (isRecording || frameCount > DESIRED_FRAMERATE * record.first) {
                if (!isRecording) {
                    println("Starting recording")
                    isRecording = true
                }
                saveFrameToOutput()
            }
            if (frameCount > DESIRED_FRAMERATE * record.last) {
                noLoop()
                println("Converting")
                convertVideo()
            }
        }
    }

    private fun saveFrameToOutput() {
        saveFrame("$OUTPUT_DIR/frame_${saveFrameCount.toString().padStart(4, '0')}")
        saveFrameCount++
    }

    private fun deleteOutputDirectory() {
        val output = File(OUTPUT_DIR)
        if (output.exists()) {
            output.deleteRecursively()
        }
    }

    private fun convertVideo() {
        Thread {
            Thread.sleep(2_000)
            ProcessBuilder(CONVERT_TO_MKV.split(' ')).start().apply {
                waitFor()
            }
            val toWebm = overrideConvertToWebmParams ?: CONVERT_TO_WEBM
            ProcessBuilder(toWebm.split(' ')).start().apply {
                waitFor()
            }
            println("Done")
        }.start()
    }

    override fun mouseClicked() {
        super.mouseClicked()
        println(mouseX, mouseY)
    }

    companion object {
        private const val DESIRED_FRAMERATE = 60
        private const val OUTPUT_DIR = "out"
        private const val CONVERT_TO_MKV =
            "ffmpeg -framerate 60 -start_number 1 -i $OUTPUT_DIR/frame_%04d.tif -an -crf 0 -b:v 0 $OUTPUT_DIR/OUT.mkv"
        private const val CONVERT_TO_WEBM =
            "ffmpeg -i $OUTPUT_DIR/OUT.mkv -c:v libvpx -an -crf 4 -b:v 1500K $OUTPUT_DIR/OUT_4.webm"
    }
}

data class GraphicComponent(
    val canvasEffects: List<CanvasEffect> = emptyList(),
    val graphicEffect: GraphicEffect? = null,
    val imageEffects: List<ImageEffect> = emptyList(),
)
