package processing.art.piece

import processing.art.effect.canvas.RotationCanvasEffect
import processing.core.PApplet
import processing.art.effect.graphic.BackgroundImageGraphicEffect
import processing.art.effect.graphic.SpinningWheelGraphicEffect
import processing.art.effect.graphic.TextGraphicEffect
import processing.art.effect.image.CircularTransparencyImageEffect
import processing.art.effect.image.LinearFunctionEraseImageEffect
import java.awt.Color

// ffmpeg -framerate 60 -start_number 0380 -i 'screen-%04d.tif' -an -crf 0 -b:v 0 OUT.mkv
// ffmpeg -i OUT.mkv -c:v libvpx -an -crf 4 -b:v 1500K -vf scale=800x580 OUT_4.webm
class TapisVertJamesCarrollBeckwith : Piece(
    pieceWidth = 1600,
    pieceHeight = 1160,
    record = null,
    effectsProvider = {
        listOf(
            GraphicComponent(graphicEffect = BackgroundImageGraphicEffect("pictures/james_carroll_beckwith.jpg")),
            GraphicComponent(
                canvasEffects = listOf(
                    RotationCanvasEffect(
                        center = 938 to 182,
                        type = RotationCanvasEffect.RotationType.Spinning(
                            startAngleRadians = 0f,
                            rotationSpeed = 0.005f,
                        )
                    )
                ),
                graphicEffect = SpinningWheelGraphicEffect(
                    strokeColor = it.color(Color.WHITE.red, Color.WHITE.green, Color.WHITE.blue),
                    strokeWidth = 12f,
                    arcLength = 10f,
                    numberOfSpikes = 12,
                    spikeSpeedModifier = 2.3f,
                    shrinkSpeed = -0.3f,
                    drawSpeed = 1.2f,
                    centerX = 938f,
                    centerY = 182f,
                    startingArc = 22.5f,
                ),
                imageEffects = listOf(
                    CircularTransparencyImageEffect(
                        fullyBlurAtDistance = 550f,
                        centerX = 938f,
                        centerY = 182f
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 953f to 3f,
                        pointTwo = 1160f to 527f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.TO_RIGHT,
                        fadeLength = 100f,
                    ),
                )
            ),
            GraphicComponent(
                graphicEffect = TextGraphicEffect(
                    textSize = 40f,
                    text = "the-rdk.github.io",
                    position = TextGraphicEffect.Position.BOTTOM_RIGHT,
                    textColor = it.color(Color.WHITE.red, Color.WHITE.green, Color.WHITE.blue)
                )
            )
        )
    }
)

fun main() {
    val sketch = TapisVertJamesCarrollBeckwith()
    PApplet.runSketch(arrayOf("TapisVertJamesCarrollBeckwith"), sketch)
}
