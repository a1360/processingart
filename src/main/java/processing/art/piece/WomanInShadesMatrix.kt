package processing.art.piece

import processing.core.PApplet
import processing.art.effect.graphic.MatrixGraphicEffect
import processing.art.effect.graphic.RandomAsianCharacterProvider
import processing.art.effect.graphic.BackgroundImageGraphicEffect
import processing.art.effect.graphic.TextGraphicEffect
import processing.art.effect.image.CircularTransparencyImageEffect
import processing.art.effect.image.LinearFunctionEraseImageEffect
import java.awt.Color
import kotlin.random.Random

// ffmpeg -framerate 60 -start_number 0380 -i 'screen-%04d.tif' -an -crf 0 -b:v 0 OUT.mkv
// ffmpeg -i OUT.mkv -c:v libvpx -an -crf 4 -b:v 1500K -vf scale=599x757 -ss 00:00:00.000 -to 00:00:15.000 OUT_4.webm
// ffmpeg -i OUT.mkv -vf scale=300x376 -an -crf 50 -ss 00:00:00.000 -to 00:00:04.000 -b:v 5K OUT_15.gif
class WomanInShadesMatrix : Piece(
    pieceWidth = 599,
    pieceHeight = 757,
    record = null,
    effectsProvider = {
        val random = Random(System.currentTimeMillis())
        listOf(
            GraphicComponent(graphicEffect = BackgroundImageGraphicEffect("pictures/women_in_shades.jpg")),
            GraphicComponent(
                graphicEffect = MatrixGraphicEffect(
                    textSize = 10f,
                    speedRange = 1 until 3,
                    random = random,
                    textColor = it.color(Color.GREEN.red + 20, Color.GREEN.green, Color.GREEN.blue + 20),
                    textHighlightColor = it.color(220f, 255f, 220f),
                    blendLength = 2 until 10,
                    rainLength = 30 until 60,
                    characterPoviderFactory =  { column, index ->
                        RandomAsianCharacterProvider(
                            random = random,
                            characterSwitchInterval = 15 until 30
                        )
                    },
                ),
                imageEffects = listOf(
                    CircularTransparencyImageEffect(
                        fullyBlurAtDistance = 250f,
                        centerX = 317f,
                        centerY = 374f,
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 73f to 319f,
                        pointTwo = 550f to 318f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.ABOVE,
                        fadeLength = 15f,
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 86f to 441f,
                        pointTwo = 540f to 425f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.BELOW,
                        fadeLength = 10f,
                    ),
                )
            ),
            GraphicComponent(
                graphicEffect = TextGraphicEffect(
                    textSize = 25f,
                    text = "the-rdk.github.io",
                    position = TextGraphicEffect.Position.BOTTOM_RIGHT,
                    textColor = it.color(Color.BLACK.red, Color.BLACK.green, Color.BLACK.blue)
                )
            )
        )
    }
)

fun main() {
    val sketch = WomanInShadesMatrix()
    PApplet.runSketch(arrayOf("WomanInShadesMatrix"), sketch)
}
