package processing.art.piece

import processing.art.effect.canvas.RotationCanvasEffect
import processing.core.PApplet
import processing.art.effect.graphic.BackgroundImageGraphicEffect
import processing.art.effect.graphic.SpinningWheelGraphicEffect
import processing.art.effect.graphic.TextGraphicEffect
import processing.art.effect.image.CircularTransparencyImageEffect
import processing.art.effect.image.LinearFunctionEraseImageEffect
import java.awt.Color

// ffmpeg -framerate 60 -start_number 0380 -i 'screen-%04d.tif' -an -crf 0 -b:v 0 OUT.mkv
// ffmpeg -i OUT.mkv -c:v libvpx -an -crf 4 -b:v 1500K -vf scale=599x757 -ss 00:00:00.000 -to 00:00:15.000 OUT_4.webm
class WomanInShadesSun : Piece(
    pieceWidth = 599,
    pieceHeight = 757,
    record = null,
    overrideConvertToWebmParams = "-vf scale=599x757 -ss 00:00:00.000 -to 00:00:15.000",
    effectsProvider = {
        listOf(
            GraphicComponent(graphicEffect = BackgroundImageGraphicEffect("pictures/women_in_shades.jpg")),
            GraphicComponent(
                canvasEffects = listOf(
                    RotationCanvasEffect(
                        center = 425 to 320,
                        type = RotationCanvasEffect.RotationType.Spinning(0f, 0.005f),
                    )
                ),
                graphicEffect = SpinningWheelGraphicEffect(
                    strokeColor = it.color(Color.WHITE.red, Color.WHITE.green, Color.WHITE.blue),
                    strokeWidth = 12f,
                    arcLength = 10f,
                    numberOfSpikes = 12,
                    spikeSpeedModifier = 2.3f,
                    shrinkSpeed = -0.3f,
                    drawSpeed = 1.7f,
                    centerX = 425f,
                    centerY = 320f,
                    startingArc = 0f,
                    distanceBetweenCircles = 75f,
                ),
                imageEffects = listOf(
                    CircularTransparencyImageEffect(
                        fullyBlurAtDistance = 200f,
                        centerX = 317f,
                        centerY = 374f,
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 73f to 319f,
                        pointTwo = 550f to 318f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.ABOVE,
                        fadeLength = 15f,
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 86f to 441f,
                        pointTwo = 540f to 425f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.BELOW,
                        fadeLength = 10f,
                    ),
                )
            ),
            GraphicComponent(
                graphicEffect = TextGraphicEffect(
                    textSize = 25f,
                    text = "the-rdk.github.io",
                    position = TextGraphicEffect.Position.BOTTOM_RIGHT,
                    textColor = it.color(Color.BLACK.red, Color.BLACK.green, Color.BLACK.blue)
                )
            )
        )
    }
)

fun main() {
    val sketch = WomanInShadesSun()
    PApplet.runSketch(arrayOf("WomenInShadesSun"), sketch)
}
