package processing.art.piece

import processing.art.effect.graphic.FishShapeGraphicEffect
import processing.art.effect.graphic.ReflectionGraphicEffect
import processing.art.effect.graphic.SlidingBarsGraphicEffect
import processing.art.effect.image.BlurImageEffect
import processing.art.effect.image.LinearFunctionEraseImageEffect
import processing.art.effect.image.MaskImageEffect
import processing.art.effect.image.PerlinNoiseImageEffect
import processing.art.effect.image.ShrinkTopHalfImageEffect
import processing.core.PApplet

class FishLineSunset : Piece(
    pieceWidth = 500,
    pieceHeight = 800,
    record = 1 until 20,
    effectsProvider = {
        listOf(
            GraphicComponent(
                graphicEffect = SlidingBarsGraphicEffect(
                    color = it.color(182, 17, 17),
                    gap = 5f,
                    thickness = 10f,
                    animationSpeed = 0.25f,
                ),
                imageEffects = listOf(
                    MaskImageEffect(
                        FishShapeGraphicEffect(
                            x = 150,
                            y = 60,
                            width = 200,
                            height = 300,
                            thickness = 30f,
                            color = 255,
                        )
                    ),
                    BlurImageEffect(16f)
                )
            ),
            GraphicComponent(
                graphicEffect = ReflectionGraphicEffect(
                    orientation = ReflectionGraphicEffect.Orientation.Horizontal.Up(400)
                ),
                imageEffects = listOf(
                    PerlinNoiseImageEffect(
                        x = 0,
                        y = 400,
                        width = 500,
                        height = 400,
                        scale = 50f,
                        speed = 0.02f,
                        thickness = 3,
                    ),
                    LinearFunctionEraseImageEffect(
                        pointOne = 6f to 400f,
                        pointTwo = 500f to 401f,
                        eraseDirection = LinearFunctionEraseImageEffect.EraseDirection.BELOW,
                        fadeLength = 420f,
                    ),
                    ShrinkTopHalfImageEffect(shrinkHeightBy = 4f / 5f)
                )
            )
        )
    }
)

fun main() {
    val sketch = FishLineSunset()
    PApplet.runSketch(arrayOf("FishLineSunset"), sketch)
}
