package processing.art.piece

import processing.art.effect.canvas.RotationCanvasEffect
import processing.core.PApplet
import processing.art.effect.graphic.MatrixGraphicEffect
import processing.art.effect.graphic.RandomAsianCharacterProvider
import processing.art.effect.graphic.WordCharacterProviderFactory
import processing.art.effect.graphic.BackgroundColorGraphicEffect
import processing.art.effect.graphic.SpinningWheelGraphicEffect
import processing.art.effect.image.CircularTransparencyImageEffect
import processing.art.toColorInt
import java.awt.Color
import kotlin.random.Random

// ffmpeg -framerate 60 -start_number 0380 -i 'screen-%04d.tif' -an -crf 0 -b:v 0 OUT.mkv
// ffmpeg -i OUT.mkv -c:v libvpx -an -crf 0 -b:v 2000K -vf scale=500x300 OUT_4.webm
class MatrixExplodingSun(
    private val random: Random = Random(System.currentTimeMillis())
) : Piece(
    pieceWidth = 1000,
    pieceHeight = 600,
    record = null,
    overrideConvertToWebmParams = "ffmpeg -i out/OUT.mkv -c:v libvpx -an -crf 4 -b:v 2000K -vf scale=500x300 out/OUT_4.webm",
    effectsProvider = {
        listOf(
            GraphicComponent(graphicEffect = BackgroundColorGraphicEffect(it.toColorInt(Color.BLACK))),
            GraphicComponent(
                graphicEffect = MatrixGraphicEffect(
                    speedRange = 1 until 3,
                    random = random,
                    textColor = it.color(Color.GREEN.red + 20, Color.GREEN.green, Color.GREEN.blue + 20),
                    textHighlightColor = it.color(220f, 255f, 220f),
                    blendLength = 2 until 10,
                    rainLength = 5 until 30,
                    hardcodedColumnFadeLength = mapOf(
                        44 to " the-rdk.github.io ".length * 2,
                        18 to " the-rdk.github.io ".length * 2
                    ),
                    characterPoviderFactory = { column, index ->
                        if (column == 44 || column == 18) {
                            WordCharacterProviderFactory(word = " the-rdk.github.io ".reversed()).requestProviderFor(
                                column,
                                index
                            )
                        } else {
                            RandomAsianCharacterProvider(
                                random = random,
                                characterSwitchInterval = 15 until 30
                            )
                        }
                    }
                ),
            ),
            GraphicComponent(
                canvasEffects = listOf(
                    RotationCanvasEffect(
                        center = 500 to 600,
                        type = RotationCanvasEffect.RotationType.Spinning(
                            0f, 0.001f,
                        )
                    )
                ),
                graphicEffect = SpinningWheelGraphicEffect(
                    strokeColor = it.color(Color.WHITE.red, Color.WHITE.green, Color.WHITE.blue),
                    strokeWidth = 12f,
                    arcLength = 10f,
                    numberOfSpikes = 12,
                    spikeSpeedModifier = 2.3f,
                    shrinkSpeed = -0.3f,
                    drawSpeed = 1.7f,
                    centerX = 500f,
                    centerY = 600f,
                ),
                imageEffects = listOf(
                    CircularTransparencyImageEffect(
                        centerX = 500f,
                        centerY = 600f,
                        550f,
                    )
                )
            )
        )
    }
)

fun main() {
    val sketch = MatrixExplodingSun()
    PApplet.runSketch(arrayOf("MatrixExplodingSun"), sketch)
}
