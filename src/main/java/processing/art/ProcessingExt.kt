package processing.art

import processing.core.PApplet
import processing.core.PImage
import java.awt.Color

internal fun PApplet.toColorInt(color: Color): Int =
    color(color.red, color.green, color.blue)

internal fun PImage.toPixelLocation(x: Int, y: Int): Int =
    x + y * width
