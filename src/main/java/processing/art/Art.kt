package processing.art

import processing.core.PApplet

// https://happycoding.io/tutorials/java/processing-in-java
class Sketch : PApplet() {

}

fun main() {
    val sketch = Sketch()
    PApplet.runSketch(arrayOf("Sketch"), sketch)
}